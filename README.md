# vector-search

The vector search is implemented as follows:
WebUI [Reflex] -> sends request to backend [FastAPI] -> execute a search query to Vector database [Milvus]


### Dataset
I decided to automatically grab some amount of articles.
I found a nice link [https://en.wikipedia.org/wiki/Wikipedia:Good_articles/By_length](https://en.wikipedia.org/wiki/Wikipedia:Good_articles/By_length) 
containing links to 2000 articles.
I fetched all of them and put into a text file with a json format for each article.
You may find logic in [fetch-wiki-data/wiki-fetch.py](fetch-wiki-data/wiki-fetch.py)
To not download data yourself, I put the ready file with articles into google cloud, you may download it using script [download_2000_articles_file.sh](download_2000_articles_file.sh)

### Vector database
I chose Milvus; it's one of the most popular vector databases, as a second option I had Quadrand, but as for me it hides a lot of details.
Both of them could be scales horizontally, Milvus also support partitions and even GPU acceleration.
As for embedding model I chose `BAAI/bge-large-en-v1.5`, it gives quite big vector size and can accept big chunks of text.
You may find logic for populating data in [bakend/init_collection_articles.py](backend/init_collection_articles.py)
Since embedding process is quite compute intensive, using Google Cloud GPU I've generated vectors for the first 100 articles from the file in the description above.
You may download it using script [download_100_articles_with_vectors.sh](download_100_articles_with_vectors.sh) and just import ready vecrots using main method from `bakend/init_collection_articles.py` 
I've could create a more nice script to do it inside Docker but I am out of time for this task.
For creating embeddings I use SentenceTransformers library, it support GPU acceleration but as downside it fetch a ton of dependencies.
The final Docker image would about 12 GB, I've could use more lightweight library from Qdrand but it doesn't support GPU acceleration.

    
### web API backend
I use FastAPI, it could be any in fact because the logic is quite simple, just one search API
    
### WEB UI
For web frontend I use nice python framework Reflex (https://reflex.dev/), it allows to create WEB UI completely in Python. In it's core it's a wrapper over ReactJS framework.


As for the bonus part it could be easily implemented using some full text search engine like TypeSense or MeiliSearch.
Population and quyering data would be really simple.
The just using response rank we can mix it with rank from vector search and we will provide a combined result.
But unfortunately I am out of time to implement it.

