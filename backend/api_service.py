
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from neural_searcher import NeuralSearcher
from text_searcher import TextSearcher

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

neural_searcher = NeuralSearcher()
text_searcher = TextSearcher()


@app.get("/api/search")
async def read_item(q: str):
    result = neural_searcher.search(text=q)
    return {"result": result}


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8888)
