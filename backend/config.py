from dotenv import load_dotenv

load_dotenv()

# CLUSTER_ENDPOINT = "http://127.0.0.1:19530"
CLUSTER_ENDPOINT = "http://vector-db:19530"
COLLECTION_NAME = "wiki"
MODEL_NAME = 'BAAI/bge-large-en-v1.5'