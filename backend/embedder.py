from typing import List

import numpy as np
import pandas as pd
import torch
from langchain.text_splitter import RecursiveCharacterTextSplitter
from pandas import DataFrame
from sentence_transformers import SentenceTransformer
from torch.nn import functional as F

import config
from utils import timeit


class Embedder:
    def __init__(self, batch_size=100):
        self.model_name = config.MODEL_NAME

        self.encoder = self._get_encoder()
        self.embedding_length = self.encoder.get_sentence_embedding_dimension()
        self.max_seq_length = self.encoder.get_max_seq_length()

        self.hf_eos_token_length = 1

        self.batch_size = batch_size

        print(f"model_name: {self.model_name}")
        print(f"EMBEDDING_LENGTH: {self.embedding_length}")
        print(f"MAX_SEQ_LENGTH: {self.max_seq_length}")

    @timeit
    def _get_encoder(self):
        torch.backends.cudnn.deterministic = True
        device = "cuda" if torch.cuda.is_available() else "cpu"
        print(f"device: {device}")

        encoder = SentenceTransformer(self.model_name, device=device)
        return encoder

    @timeit
    def chunk_text(
        self, df: pd.DataFrame, chunk_size: int, chunk_overlap: int
    ) -> pd.DataFrame:
        """
        Split the text into smaller chunks fitting into the model length
        """

        batch: DataFrame = df
        print(f"original shape: {batch.shape}")

        batch["chunk"] = batch["cleaned_text"].apply(
            self.recursive_splitter_wrapper,
            chunk_size=chunk_size,
            chunk_overlap=chunk_overlap,
        )

        batch = batch.explode("chunk", ignore_index=True)
        print(f"new shape: {batch.shape}")

        review_embeddings = torch.tensor(self.encoder.encode(batch["chunk"]))
        review_embeddings = F.normalize(review_embeddings, p=2, dim=1)

        converted_values = list(map(np.float32, review_embeddings))
        batch["vector"] = converted_values

        batch.drop(columns=["text", "cleaned_text"], inplace=True)

        return batch

    @timeit
    def embed_query(
        self, query: List[str]
    ) -> list[np.float32]:

        vectors = torch.tensor(self.encoder.encode(query))
        vectors_normalized = F.normalize(vectors, p=2, dim=1)
        result = list(map(np.float32, vectors_normalized))

        return result

    @timeit
    def recursive_splitter_wrapper(self, text, chunk_size, chunk_overlap) -> List[str]:
        chunk_overlap = np.round(chunk_size * 0.10, 0)

        text_splitter = RecursiveCharacterTextSplitter(
            chunk_size=chunk_size,
            chunk_overlap=chunk_overlap,
            length_function=len,
        )

        chunks: List[str] = text_splitter.split_text(text)

        return chunks
