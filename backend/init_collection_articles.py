import json

import numpy as np
import pandas as pd
from pymilvus import MilvusClient, MilvusException

import config
from embedder import Embedder
from utils import timeit


class DataImporter:
    def __init__(self):
        self.embedder = Embedder()
        self.collection_name = config.COLLECTION_NAME
        self.db_client = MilvusClient(
            collection_name=self.collection_name,
            uri=config.CLUSTER_ENDPOINT,
            # vector_field="float_vector",
            overwrite=True,
        )

    @timeit
    def _drop_create_db_collection(self):
        # Add custom HNSW search index to the collection.
        # M = max number graph connections per layer. Large M = denser graph.
        # Choice of M: 4~64, larger M for larger data and larger embedding lengths.
        max_graph_conn_per_layer = 16

        # efConstruction = num_candidate_nearest_neighbors per layer.
        # Use Rule of thumb: int. 8~512, efConstruction = M * 2.
        ef_construction = max_graph_conn_per_layer * 2

        # Create the search index for Milvus server.
        index_params = dict(
            {"M": max_graph_conn_per_layer, "efConstruction": ef_construction}
        )
        index = {
            "index_type": "HNSW",
            "metric_type": "COSINE",
            "params": index_params,
        }

        if self.collection_name in self.db_client.list_collections():
            self.db_client.drop_collection(self.collection_name)
            print(f"Successfully dropped collection: `{self.collection_name}`")

        self.db_client.create_collection(
            self.collection_name,
            self.embedder.embedding_length,
            consistency_level="Eventually",
            auto_id=True,
            overwrite=True,
            params=index,
        )

        print(f"Created collection: {self.collection_name}")
        print(self.db_client.describe_collection(self.collection_name))

    @timeit
    def import_from_json_file(self, file_path: str):
        self._drop_create_db_collection()

        df_articles = self._read_into_df(file_path)

        chunk_size = self.embedder.max_seq_length - self.embedder.hf_eos_token_length
        chunk_overlap = np.round(chunk_size * 0.10, 0)

        # TODO: create generator for batches
        batch = self.embedder.chunk_text(df_articles, chunk_size, chunk_overlap)

        self._persist_into_db(batch)

    @timeit
    def import_from_pickle(self, file_path: str):
        self._drop_create_db_collection()
        batch: pd.DataFrame = pd.read_pickle(file_path)
        self._persist_into_db(batch)

    @timeit
    def _persist_into_db(self, batch):
        dict_list = [row.to_dict() for _, row in batch.iterrows()]

        try:
            self.db_client.insert(
                self.collection_name, data=dict_list, batch_size=10_000, progress_bar=True
            )
            self.db_client.flush(self.collection_name)
        except MilvusException as e:
            print(f"Error: {e}")

    @timeit
    def _read_into_df(self, file_path) -> pd.DataFrame:
        articles: list[dict] = []
        with open(file_path, "r") as file:
            for line in file:
                articles.append(json.loads(line))

        df: pd.DataFrame = pd.DataFrame(articles[:1])

        print(f"Number of articles: {len(df)}")

        return df


def main():
    # DataImporter().import_from_json_file(ARTICLES_FILE_PATH)
    DataImporter().import_from_pickle("data/batch_100_article_vectors.pkl")
    # pass


if __name__ == "__main__":
    main()
