import pprint
from typing import List

from pymilvus import MilvusClient

import config
from embedder import Embedder


class NeuralSearcher:

    def __init__(self):
        self.embedder = Embedder()
        self.collection_name = config.COLLECTION_NAME

        self.db_client = MilvusClient(
            collection_name=self.collection_name,
            uri=config.CLUSTER_ENDPOINT,
            overwrite=True,
        )

    def search(self, text: str, filter_: dict = None) -> List[dict]:
        query = [text]
        query_embeddings = self.embedder.embed_query(query)

        max_graph_conn_per_layer = 16

        # efConstruction = num_candidate_nearest_neighbors per layer.
        # Use Rule of thumb: int. 8~512, efConstruction = M * 2.
        ef_construction = max_graph_conn_per_layer * 2

        output_fields = ["title", "link", "chunk"]
        search_params = {"ef": ef_construction}

        results = self.db_client.search(
            self.collection_name,
            data=query_embeddings,
            search_params=search_params,
            output_fields=output_fields,
            limit=10,
            consistency_level="Eventually",
        )[0]

        pprint.pprint(results)

        return results


def main():
    # DataImporter().import_from_json_file(ARTICLES_FILE_PATH)
    # DataImporter().import_from_pickle("data/batch_article_vectors_model_b_3.pkl")

    # DataImporter().query()
    ns = NeuralSearcher()
    r = ns.search("famous singer")
    # r = ns.search("I am a programmer, advice me an article to read")
    print(r)


if __name__ == "__main__":
    main()
