
import config


class TextSearcher:
    def __init__(self):
        # self.highlight_field = TEXT_FIELD_NAME
        self.collection_name = config.COLLECTION_NAME
        self.client = None

    def highlight(self, record, query) -> dict:
        text = record[self.highlight_field]

        # for word in query.lower().split():
        #     if len(word) > 4:
        #         pattern = re.compile(
        #             rf"(\b{re.escape(word)}?.?\b)",
        #             flags=re.IGNORECASE
        #         )
        #     else:
        #         pattern = re.compile(
        #             rf"(\b{re.escape(word)}\b)",
        #             flags=re.IGNORECASE
        #         )
        #     text = re.sub(pattern, r"<b>\1</b>", text)

        record[self.highlight_field] = text
        return record

    def search(self, query, top=5):
        return None
