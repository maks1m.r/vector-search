import time
from textwrap import dedent

import cleantext


def clean_text(text: str) -> str:
    cleaned_text = cleantext.clean(
        text,
        fix_unicode=True,
        lower=False,
        no_line_breaks=False,
        no_urls=True,
        no_emails=True,
        no_phone_numbers=True,
        no_numbers=False,
        no_digits=False,
        no_currency_symbols=False,
        no_punct=False,
        replace_with_punct="",
        replace_with_url="<URL>",
        replace_with_email="<EMAIL>",
        replace_with_phone_number="<PHONE>",
        replace_with_number="<NUMBER>",
        replace_with_digit="<DIGIT>",
        replace_with_currency_symbol="<CUR>",
        lang="en"
    )
    return cleaned_text


def timeit(func):
    nanos = 1_000_000_000

    def wrapper(*args, **kwargs):
        start = time.perf_counter_ns()
        result = func(*args, **kwargs)
        end = time.perf_counter_ns()
        print(dedent(
            f"""
            <<<
            Time taken for {func.__name__}: {(end - start) / nanos:0.4f} seconds
            >>>
            """
        ))
        return result

    return wrapper
