import json
import os
from concurrent.futures import ThreadPoolExecutor

import pywikibot
import requests
import wikitextparser
from bs4 import BeautifulSoup

MAX_CONCURRENT_DOWNLOADS = 5

# URL of the Wikipedia page containing the article links
URL = "https://en.wikipedia.org/wiki/Wikipedia:Good_articles/By_length"
ARTICLE_LINKS_FILE_NAME = "articles/article_links.csv"
ARTICLES_JSON_FILE = "articles/articles.json"
ARTICLES_JSON_TXT_FILE = "articles/articles.json.txt"


class ArticleDownloader:
    """
    Class helps to download 2000 articles from Wikipedia
    """

    def __init__(self, url: str):
        self.url = url
        self.article_links: list[tuple[str, str]] = []
        self.articles_with_text: list[dict] = []

    def _get_links_from_url(self):
        """
        Get WiKi article links from the given URL
        :param url: url with links to articles
        :return: list of tuples containing article title and link
        """
        response = requests.get(self.url)

        soup = BeautifulSoup(response.text, "html.parser")
        tables_in_page = soup.find_all("table")

        self.article_links = []

        for table in tables_in_page:
            links_in_table = table.find_all("a", href=True, recursive=True)
            for link in links_in_table:
                href = link["href"]
                title = link["title"]
                if (
                        href.startswith("/wiki/")
                        and not href.startswith("/wiki/Wikipedia:")
                        and not href.startswith("/wiki/Special:")
                ):
                    full_url = f"https://en.wikipedia.org{href}"
                    self.article_links.append((title, full_url))

        print(f"Found {len(self.article_links)} article links")

    def _save_links_to_csv_file(self):
        directory = os.path.dirname(ARTICLE_LINKS_FILE_NAME)
        if not os.path.exists(directory):
            os.makedirs(directory)

        with open(ARTICLE_LINKS_FILE_NAME, "w") as file:
            file.write('title|link\n')
            for title, link in self.article_links:
                file.write(f'{title}|{link}\n')

    def _download_article(self, title: str, link: str):
        print(f"Downloading {link} ....")
        site = pywikibot.Site("en", "wikipedia")
        page_title = link.split("/")[-1]
        wiki_page = pywikibot.Page(site, page_title)

        if wiki_page.isRedirectPage():
            wiki_page = wiki_page.getRedirectTarget()

        parsed_sections = wikitextparser.parse(wiki_page.text).sections

        full_text = ''.join([
            section.plain_text().strip() for section in parsed_sections
        ])

        article_data = {
            "title": title,
            "link": link,
            "text": full_text
        }
        self.articles_with_text.append(article_data)
        print(f"Downloaded {link}")

    def _download_articles(self):
        self.articles_with_text = []
        with ThreadPoolExecutor(max_workers=MAX_CONCURRENT_DOWNLOADS) as executor:
            executor.map(lambda args: self._download_article(*args), self.article_links)

    def _save_to_json_txt(self):
        with open(ARTICLES_JSON_TXT_FILE, "w", encoding="utf-8") as file:
            articles = [
                json.dumps(article, ensure_ascii=False) + '\n'
                for article
                in self.articles_with_text
            ]
            file.writelines(articles)

    def download(self):
        self._get_links_from_url()
        self._save_links_to_csv_file()
        self._download_articles()
        # self._save_to_json()
        self._save_to_json_txt()


if __name__ == "__main__":
    ArticleDownloader(URL).download()
