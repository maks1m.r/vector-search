import reflex as rx


config = rx.Config(
    app_name="wiki",
    env=rx.Env.DEV,
    frontend_port=8000,
    backend_port=8001
)
