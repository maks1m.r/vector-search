import requests
from requests import Response
from wiki.state import State
from wiki.state.models import Article
import reflex as rx


class ArticleState(State):
    articles: list[Article] = []
    query = ""

    def get_articles(self) -> list[Article]:
        response: Response = requests.get(
            f"http://127.0.0.1:8888/api/search?q={self.query}"
        )

        articles_raw = response.json()["result"]

        print(articles_raw)

        self.articles = [
            Article(
                title=row["entity"]["title"],
                link=row["entity"]["link"],
                text=row["entity"]["chunk"],
            )
            for row in articles_raw
        ]

    def filter(self, query):
        self.query = query
        print("Returning...")
        return self.get_articles()

    def handle_change(self, value: str):
        self.query = value


def article_row(article: Article):
    return rx.chakra.tr(
        rx.chakra.td(
            rx.vstack(
                rx.heading(article.title, as_="h5"),
                rx.link(article.link, href=article.link),
                rx.text(article.text),
            )
        )
    )


def article():
    return rx.chakra.box(
        rx.chakra.hstack(
            rx.chakra.heading("Articles"),
            justify_content="space-between",
            align_items="flex-start",
            margin_bottom="1rem",
        ),
        rx.chakra.hstack(
            rx.chakra.input(
                placeholder="Enter search phrase...",
                # on_change=ArticleState.filter
                on_change=ArticleState.handle_change,
            ),
            rx.chakra.button("Refresh", on_click=ArticleState.get_articles),
            
        ),
        rx.chakra.table_container(
            rx.chakra.table(
                rx.chakra.tbody(rx.foreach(ArticleState.articles, article_row))
            ),
            margin_top="1rem",
        ),
        width="100%",
        max_width="960px",
        padding_x="0.5rem",
    )
