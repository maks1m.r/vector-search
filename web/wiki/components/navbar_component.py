import reflex as rx


def navbar():
    return rx.chakra.box(
        rx.chakra.hstack(
            rx.chakra.hstack(rx.chakra.box()),
            rx.chakra.button(
                rx.chakra.icon(tag="moon"),
                on_click=rx.toggle_color_mode,
            ),
            justify_content="space-between",
        ),
        width="100%",
        padding="1rem",
        margin_bottom="2rem",
        border_bottom="1px solid black",
    )
