from wiki.components import navbar
from wiki.components import article
import reflex as rx


def index():
    return rx.chakra.vstack(
        navbar(),
        article(),
        spacing="1.5rem",
    )
