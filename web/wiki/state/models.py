import reflex as rx
from dataclasses import dataclass

# @dataclass
class Article(rx.Model):
    title: str
    link: str
    text: str