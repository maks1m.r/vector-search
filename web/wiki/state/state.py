from typing import Optional
import reflex as rx


class State(rx.State):
    """The app state."""

    pass