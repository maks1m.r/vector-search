"""Welcome to Reflex! This file outlines the steps to create a basic app."""
import reflex as rx
from wiki.pages import index
from wiki.state import State


# Add pages to the app.
app = rx.App()
app.add_page(index)